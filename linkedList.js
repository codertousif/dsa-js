// Singly Linked List

class Node {
  // a default node
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class LinkedList {
  constructor(value) {
    this.head = new Node(value);
    this.tail = this.head;
    this.length = 1;
  }
  append(value) {
    // Add a node to the list at the end
    const newNode = new Node(value);
    this.tail.next = newNode;
    this.tail = newNode;
    this.length++;
    return this;
  }

  prepend(value) {
    // Add a node to the list at the start
    const newNode = new Node(value);
    newNode.next = this.head;
    this.head = newNode;
    this.length++;
    return this;
  }

  printList() {
    // Displays list of values in the list
    let presentNode = this.head;
    const array = [];
    while(presentNode !== null) {
      array.push(presentNode.value);
      presentNode = presentNode.next;
    }
    return array;
  }

  insert(index, value){
    // Inserts a node at given index
    if(index >= this.length) {
      console.log('yes');
      this.append(value);
      return this.printList();
    }
    const newNode = new Node(value);
    let presentNode = this.traverseList(index-1);
    let previousNode = presentNode;
    presentNode = presentNode.next;
    newNode.next = presentNode;
    previousNode.next = newNode; 
    this.length++;
  }

  traverseList(index) {
    // Traveses the list
    if(index >= this.length) {
      return 'Out of bound traverseList()';
    }
    let presentNode = this.head;
    for(let i=0; i<index; i++) {
      presentNode = presentNode.next;
    }
    return presentNode;
  }

  removeNode(index) {
    // Removes a node from the list 
    if (index >= this.length-1 || index <= 0) {
      return console.log('Index out of Bound');
    }
    let presentNode = this.traverseList(index-1);
    console.log(presentNode);
    let deleteNode = presentNode.next;
    presentNode.next = deleteNode.next;
    this.length--;
    return this.printList();
  }

  reverseList() {
    // Reverses the linked list
    if (!this.head.next) {
      return this.head;
    }

    let first = this.head;
    let second = first.next;
    first.next = null;
    this.tail = first;
    while(second !== null) {
      let temp = second.next;
      second.next = first;
      first = second;
      second = temp;
    }
    this.head = first;
    return this.printList();
  }
}

let myLinkedList = new LinkedList(10);
myLinkedList.append(5);
myLinkedList.append(16);
myLinkedList.prepend(1);

myLinkedList.insert(2, 99);
myLinkedList.insert(20, 88)

console.log(myLinkedList.printList());

// console.log(myLinkedList.removeNode(5));

// console.log(myLinkedList);

// console.log(myLinkedList.printList());
// console.log(myLinkedList.traverseList(50));

console.log(myLinkedList.reverseList());