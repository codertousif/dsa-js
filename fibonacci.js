// Given a number N return the index value of the Fibonacci sequence, where the sequence is:

// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144 ...
// the pattern of the sequence is that each value is the sum of the 2 previous values, that means that for N=5 → 2+3

//For example: fibonacciRecursive(6) should return 8
function fibonacciIterative2(n) {
    let arr = [0, 1];
    for (let i = 2; i < n + 1; i++) {
        arr.push(arr[i - 2] + arr[i - 1]);
    }
    // console.log(arr);
    return arr[n];
}

function fibonacciIterative1(n) {
    first = 0;
    second = 1;
    console.log(first, second);
    for (let i = 2; i < n + 1; i++) {
        let temp = second;
        second = second + first;
        console.log(second);
        first = temp;
    }
}
// console.log(fibonacciIterative2(15));

// fibonacciIterative1(15);

function fibonacciRecursive(n) {
    // debugger;

    if (n < 2) {
        return n;
    }
    return fibonacciRecursive(n - 2) + fibonacciRecursive(n - 1);
}

console.log(fibonacciRecursive(4));
