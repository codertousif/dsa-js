function bubbleSort(arr) {
    // O(n^2)
    const t0 = Date.now();
    for (let i = arr.length - 1; i >= 0; i--) {
        for (let j = 0; j < i; j++) {
            if (arr[j] > arr[j + 1]) {
                let temp = arr[j + 1];
                arr[j + 1] = arr[j];
                arr[j] = temp;
            }
        }
    }

    console.log(`${Date.now() - t0} ms`);
    // for (let i = 0; i <= arr.length; i++) {
    //     for (let j = i; j < arr.length; j++) {
    //         if (arr[i] > arr[j]) {
    //             let temp = arr[j];
    //             arr[j] = arr[i];
    //             arr[i] = temp;
    //         }
    //     }
    // }
    return arr;
}

function selectionSort(arr) {
    // O(n^2)
    const t0 = Date.now();
    for (let i = 0; i < arr.length; i++) {
        // set current index as minimum
        let min = i;
        let temp = arr[i];
        for (let j = i + 1; j < arr.length; j++) {
            if (arr[j] < arr[min]) {
                // set current index as minimum
                min = j;
            }
        }
        arr[i] = arr[min];
        arr[min] = temp;
    }
    console.log(`${Date.now() - t0} ms`);
    return arr;
}

function insertionSort(array) {
    const t0 = Date.now();
    const length = array.length;
    for (let i = 0; i < length; i++) {
        if (array[i] < array[0]) {
            //move number to the first position
            array.unshift(array.splice(i, 1)[0]);
        } else {
            // only sort number smaller than number on the left of it.
            // This is the part of insertion sort that makes it fast if the array is almost sorted.
            if (array[i] < array[i - 1]) {
                //find where number should go
                for (var j = 1; j < i; j++) {
                    if (array[i] >= array[j - 1] && array[i] < array[j]) {
                        //move number to the right spot
                        array.splice(j, 0, array.splice(i, 1)[0]);
                    }
                }
            }
        }
    }
    console.log(`${Date.now() - t0} ms`);
    return array;
}

function mergeSort(arr) {
    if (arr.length == 1) {
        return arr;
    }
    const middle = Math.floor(arr.length / 2);
    const left = arr.slice(0, middle);
    const right = arr.slice(middle);

    return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
    const result = [];
    let i = 0,
        j = 0;
    while (i < left.length && j < right.length) {
        if (left[i] < right[j]) {
            result.push(left[i]);
            i++;
        } else {
            result.push(right[j]);
            j++;
        }
    }
    return result.concat(left.slice(i)).concat(right.slice(j));
}

function quickSort(array, left, right) {
    if (left < right) {
        // pi is partition index
        let pi = partition(array, left, right);

        //sort left and right
        quickSort(array, left, pi - 1);
        quickSort(array, pi + 1, right);
    }
    return array;
}

function partition(array, pivot, left, right) {
    // let pivot = array[right];
    let i = left; // index of smaller element
    // loop from arr[start] to arr[end-1]
    for (let j = left; j < right; j++) {
        if (array[j] < pivot) {
            swap(array, j, i);
            i++;
        }
    }
    // placing pivot at correct position by swapping
    swap(array, right, i);
    return i;
}

function swap(array, firstIndex, secondIndex) {
    var temp = array[firstIndex];
    array[firstIndex] = array[secondIndex];
    array[secondIndex] = temp;
}

// const arr = [69, 2, 73, 13, 37, 56, 91]
// const arr = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];
const arr = [];
for (let i = 0; i < 1000; i++) {
    arr.push(Math.floor(Math.random() * 1000));
}

// const arr = [1, 1, 1, 0, 0, 0, 0, 1, 1, 1];

// bubbleSort(arr);
// selectionSort(arr);
// insertionSort(arr)
// mergeSort(arr)

// console.log(mergeSort(arr))
//Select first and last index as 2nd and 3rd parameters
// quickSort(numbers, 0, numbers.length - 1);

// console.log('sorting:', bubbleSort(arr));
// console.log('sorting:', selectionSort(arr));
// console.log('sorting:', insertionSort(arr));
console.log('sorting:', mergeSort(arr));
// console.log('sorting:', quickSort(numbers, 0, numbers.length - 1));
