const groupAnagrams = (strs) => {
    const map = new Map();

    for (const str of strs) {
        // split each char then sort them then join them together
        // cat => [c,a,t] -> [a,c,t] -> act
        const sortedStr = [...str].sort().join('');
        // check if map already has sortedStr
        if (map.has(sortedStr)) {
            // set key to sortedStr and push str as an element of the array to the map
            // {sortedStr: [prevStr, str]}
            map.get(sortedStr).push(str);
        } else {
            // {sortedStr: [str]}
            map.set(sortedStr, [str]);
        }
    }

    return [...map.values()];
};

const groupAnagrams2 = (strs) => {
    const map = new Map();
    for (let i = 0; i < strs.length; i++) {
        const sortedStr = [...strs[i]].sort().join('');
        if (map.has(sortedStr)) {
            // set key to sortedStr and push index of str as an element of the array to the map
            // {sortedStr: [prevIdx, i]}
            map.get(sortedStr).push(i + 1);
        } else {
            // {sortedStr: [i]}
            map.set(sortedStr, [i + 1]);
        }
    }
    return [...map.values()];
};

// console.log(groupAnagrams(['cat', 'dog', 'god', 'tca']));
console.log(
    groupAnagrams2([
        'abcd',
        'dcba',
        'dcba',
        'abcd',
        'abcd',
        'adbc',
        'dabc',
        'adcb',
    ])
);
