// const find3Numbers = (arr, sum) => {
//     for (let i = 0; i < arr.length; i++) {
//         const s = [];
//         let curr_sum = sum - arr[i];

//         for (let j = i + 1; j < arr.length; j++) {
//             if (s.includes(curr_sum - arr[j])) {
//                 console.log(
//                     `Triplet is ${arr[i]}, ${arr[j]}, ${curr_sum - arr[j]} `
//                 );
//                 return true;
//             }
//             s.push(arr[j]);
//         }
//     }
//     return false;
// };

// // Driver program to test above function
// // const arr = [1, 4, 45, 6, 10, 8];
// const arr = [-1, 2, 1, -4];
// const sum = 1;

// console.log(find3Numbers(arr, sum));

const findAllSolutionsTwoPoint = (nums) => {
    const resArr = [];
    const len = nums.length;

    for (let i = 0; i < len; i++) {
        if (i !== 0 && nums[i] === nums[i - 1])
            // Skip numbers if they are the same
            continue;

        let j = i + 1;
        let k = len - 1;

        while (j < k) {
            if (nums[i] + nums[j] + nums[k] === 0) {
                resArr.push([nums[i], nums[j], nums[k]]); // Add to array result
                j++;
                while (
                    j < k &&
                    nums[j] === nums[j - 1] // Skip numbers if they are the same
                )
                    j++;
            } else if (nums[i] + nums[j] + nums[k] < 0) {
                // If numbers are too low advance left pointer
                j++;
            } else {
                // Otherwise numbers are bigger than 0 reduce right pointer
                k--;
            }
        }
    }

    return resArr; // O(n ^ 2)
};

const threeSum = function (nums) {
    if (!nums || nums.length < 3) return [];

    nums.sort((a, b) => a - b); // O(n log n)

    return findAllSolutionsTwoPoint(nums);
    /** 
		This problem in particular ends up being one that the O notations can look either
		O ( n log n + n^2) = O(n^2)
		O ( n log n + n^3) = O(n^3)
		This happens since the largest term that grows faster is the square or the cube meaning that the sort is meaningless for time complexity. 
	**/
};

const arr = [-1, 0, 1, 2, -1, -4];
const sum = 1;
console.log(threeSum(arr));
