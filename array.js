// log all pair of array

const allPair = (arr) => {
    for (let i = 0; i < arr.length; i++) {
        for (let j = 0; j < arr.length; j++) {
            console.log(arr[i], arr[j]);
        }
    }
};

const isPrime = (num) => {
    if (num < 2) {
        return 'not prime';
    }
    const maxFactor = Math.floor(num / 2);

    for (let i = 2; i < maxFactor; i++) {
        console.log(i);
        if (num % i === 0) {
            return 'not prime';
        }
    }
    return 'prime';
};

// driver coder
const array = [];

for (let i = 0; i < 5; i++) {
    array.push(Math.floor(Math.random() * 10));
}
// allPair(array);
console.log(isPrime(31));
