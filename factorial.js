/**
 * 1! = 1; 2! = 2 * 1!; 3! = 3 * 2!; 4! = 4 * 3!; 5! = 5 * 4!
 *
 */

function facItr(num) {
    if (num < 2) {
        return 1;
    }
    for (let i = num - 1; i > 1; i--) {
        num = num * i;
    }
    return num;
}

let cal = 0;

function factorial(num) {
    cal++;
    if (num < 2) {
        return 1;
    } else {
        return num * factorial(num - 1);
    }
}

function facto(num) {
    arr = [1, 1];
    for (let i = 2; i < num + 1; i++) {
        arr.push(arr[i - 1] * i);
    }
    return arr.pop();
}

function factorialCached() {
    // private cache
    const cache = {};

    return function factorial(num) {
        cal++;
        if (num in cache) {
            return cache[num];
        } else if (num < 2) {
            return 1;
        } else {
            cache[num] = num * factorial(num - 1);
            return cache[num];
        }
    };
}

// console.log(facto(10));
// facto = factorialCached()
console.log(facItr(25));
// console.log(facto(20));
// console.log(factorial(20))
// console.log(cal);

// console.log(facto(25));
// console.log(factorial(25))
// console.log(cal);

// function factorial(4) {
//   debugger;
//   if (num == 1 || num == 0) {
//     return num;
//   }
//   return 4 * factorial(3)
//   function factorial(3) {
//     debugger;
//     if (num == 1 || num == 0) {
//       return num;
//     }
//     return 3 * factorial(2)
//     function factorial(2) {
//       debugger;
//       if (num == 1 || num == 0) {
//         return num;
//       }
//       return 2 * factorial(1)
//       function factorial(1) {
//         debugger;
//         if (1 == 1 || num == 0) {
//           return 1;
//         }
//         return num * factorial(num)
//       }
//     }
//   }
// }
