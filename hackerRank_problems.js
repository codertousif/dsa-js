function diagonalDifference(arr) {
    // Write your code here
    // sum = arr[0][0] + arr[1][1] + arr[2][2];
    // sum = arr[0][2] + arr[1][1] + arr[2][0];
    let sum1 = 0,
        sum2 = 0;
    let length = arr.length
    for (let i = 0; i < length; i++) {
        sum1 = sum1 + arr[i][i];
    }
    for (let i = 0, j = length - 1; i < length, j >= 0; i++, j--) {
        sum2 += arr[i][j];
    }
    let diff = sum1 - sum2;
    return Math.abs(diff);
}

// process.stdout.write("hello: ");

// let arr = [
//     [1, 2, 37],
//     [7, 8, 9],
//     [4, 5, 69]
// ]
// console.log(diagonalDifference(arr));

function staircase(n) {
    let sp = " ",
        sh = "#";

    for (let i = 1; i < n + 1; i++) {
        for (var j = 0; j < n - i; j++) {
            process.stdout.write(sp)
        }

        for (let k = 0; k < i; k++) {
            process.stdout.write(sh)
        }
        console.log()
    }

}

// staircase(4)

function timeConversion(s) {
    /*
     * 07:05:45PM
     * slice the string by :
     * check the am pm
     * if it is pm then add 12 
     */
    // const arr = s.split(":");
    const format = s.slice(8);
    let time = parseInt(s.slice(0, 2));
    const rest = s.slice(2, 8);
    // console.log(format);
    // console.log(time);
    // console.log(rest);

    if (time < 12 && format === "PM") {
        time += 12;
    } else if (time === 12 && format === "AM") {
        time = "00"
    } else if (time < 10) {
        time = "0" + time;
    }
    time = time + rest;
    return time;
}


// console.log(timeConversion("12:45:54PM"));



// let diff = Math.abs(7 - 9)
// console.log(diff);
// let length = Math.sqrt(arr.length)
// console.log(length);


function round_to_precision(x, precision) {
    var y = +x + (precision === undefined ? 0.5 : precision / 2);
    return y - (y % (precision === undefined ? 1 : +precision));
}

// console.log(round_to_precision(11, 2)); // outputs 12
// console.log(round_to_precision(11, 3)); // outputs 12
// console.log(round_to_precision(11, 4)); // outputs 12
// console.log(round_to_precision(11, 5)); // outputs 10

function roundMultipleOf5(nums) {
    const rounded = [];

    for (let i = 0; i < nums.length; i++) {
        let element = nums[i];

        let lastDigit = element % 10;
        let firstDigit = Math.floor(element / 10);

        if (firstDigit >= 4) {
            if (lastDigit < 5 && lastDigit >= 3) {
                element = firstDigit + "5"
                console.log(firstDigit);
                console.log(element);
            } else if (lastDigit > 7) {
                firstDigit++;
                element = (firstDigit) + "0";
            }
        } else if (element > 37) {
            element = 40
        }

        rounded.push(parseInt(element));
    }

    return rounded;
}

arr2 = [80, 96, 18, 73, 78, 60, 60, 15, 45, 15, 10, 5, 46, 87, 33, 60, 14, 71, 65, 2, 5, 97, 0]

// console.log(roundMultipleOf5(arr2));

// function kangaroo(x1, v1, x2, v2) {
//     let i = 0;
//     let first = x1,
//         second = x2;

//     while (i < 9999) {
//         first += v1;
//         second += v2;
//         if (first === second) {
//             return 'YES';
//         }
//         i++;
//     }
// }

function kangaroo(x1, v1, x2, v2) {
    if (v1 > v2) {

        let remainder = (x1 - x2) % (v2 - v1);

        if (remainder == 0) {
            return "YES";
        }
    }
    return "NO";
}

// console.log(kangaroo(0, 3, 4, 2));

function breakingRecords(scores) {
    let maxScore = scores[0],
        minScore = scores[0];
    let maxScoreChanged = 0,
        minScoreChanged = 0;

    for (let i = 1; i < scores.length; i++) {
        const element = scores[i];
        if (element > maxScore) {
            maxScoreChanged++;
            maxScore = element;

        } else if (element < minScore) {
            minScoreChanged++;
            minScore = element;
        }
    }
    return [maxScoreChanged, minScoreChanged];

}

// const arr = [3, 4, 21, 36, 10, 28, 35, 5, 24, 42];
// console.log(breakingRecords(arr));

function birthday(s, d, m) {
    // m -- no of chocos she wants to give
    // d -- summing value
    // [1, 2, 1, 3, 2]
    // 1 +2 , 2+1
    // add m no of consecutive elements in the array
    // then check if it is equals to the d
    // if it is then increase result
    // continue to the next iteration 
    let result = 0;
    for (let index = 0; index < s.length; index++) {
        let chocos = 0
        if (s[index] <= d) {
            let k = 0;
            for (let j = index; k < m; j++) {
                chocos += s[j];
                k++;
            }
        }
        if (chocos === d) {
            result++;
        }
    }
    return result;
}

let arr2 = [1, 2, 1, 3, 2];
// console.log(birthday(arr2, 3, 2));


function divisibleSumPairs(n, k, arr) {

    // loop for elements of the array
    //then a nest loop which starts from next index 
    // inside that add loop1 element with loop2 element
    // then check if the sum divisible by k if true then increment result
    const hash = {};
    let result = 0;
    // const half = Math.floor(n / 2);
    for (let index = 0; index < n; index++) {

        for (let j = index + 1; j < n; j++) {
            let sum = arr[index] + arr[j];

            if (sum % k === 0) {
                hash[result] = `${arr[index]} ${arr[j]}`;
                result++;
            }
        }
    }
    console.log(hash);
    return result;
}

// let arr = [1, 3, 2, 6, 1, 2];
// let arr = [5, 9, 10, 7, 4]
// console.log(divisibleSumPairs(6, 3, arr));
// console.log(divisibleSumPairs(5, 2, arr));


function extraLongFactorials(n) {
    const arr = [1, 1]

    for (let i = 2; i <= n; i++) {
        let fac = BigInt(BigInt(arr[i - 1]) * BigInt(i));
        arr.push(fac);
    }

    return arr[n];
}

// console.log(extraLongFactorials(25));