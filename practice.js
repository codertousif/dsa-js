function factorial(num) {
    // 4! = 1 * 2 * 3 * 4 n>0
    // 2! = 1*2
    const arr = [1, 1];

    for (let i = 2; i <= num; i++) {
        let result = i * arr[i - 1];
        arr.push(result);
    }
    return arr;
}

// console.log(factorial(4));

function fibo(num) {
    const arr = [0, 1];
    let sum = 1;
    for (let i = 2; i <= num; i++) {
        let element = arr[i - 2] + arr[i - 1];
        sum = sum + element;
        arr.push(element);
    }
    console.log(sum);
    return arr;
}

console.log(fibo(5));
