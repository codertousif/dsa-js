const findRedundantBrackets = (str) => {
    const stack = [];
    for (const ch of str) {
        if (ch === ')') {
            let top = stack.pop();
            // let elementInside = 0;
            let flag = true;
            while (top !== '(') {
                if (top === '+' || top === '-' || top === '*' || top === '/') {
                    flag = false;
                }
                // elementInside += 1;
                top = stack.pop();
            }
            if (flag === true) return 1;
        } else {
            stack.push(ch);
        }
    }
    return 0;
};

// (((a+(b))+(c+d)))
console.log(findRedundantBrackets('(a)'));
