// add a method insert() to the linked list that adds a node to the specified index.
class Node {
  constructor(value) {
    this.prev = null;
    this.value = value;
    this.next = null;
  }
}

class DoublyLinkedList {
  constructor(value) {
    this.head = new Node(value);
    this.tail = this.head;
    this.length = 1;
  }
  append(value) {
    const newNode = new Node(value);
    newNode.prev = this.tail;
    this.tail.next = newNode;
    this.tail = newNode; 
    this.length++;
    return this;
  }
  prepend(value) {
    const newNode = new Node(value);
    newNode.next = this.head;
    this.head.prev = newNode;
    this.head = newNode;
    this.length++;
    return this;
  }
  printList() {
    let presentNode = this.head;
    const array = [];
    while(presentNode !== null) {
      array.push(presentNode.value);
      presentNode = presentNode.next;
    }
    return array;
  }
  insert(index, value){
    //Code here
    if(index >= this.length) {
      // console.log('Caught and Handled');
      this.append(value);
      return this.printList();
    }
    const newNode = new Node(value);
    let previousNode = this.traverseList(index-1);
    let presentNode = previousNode.next;
    newNode.prev = previousNode;
    newNode.next = presentNode;
    previousNode.next = newNode; 
    presentNode.prev = newNode;
    this.length++;
    return this.printList();
  }

  traverseList(index) {
    let presentNode = this.head;
    for(let i=0; i<index; i++) {
      presentNode = presentNode.next;
    }
    return presentNode;
  }

  removeNode(index) {
    if (index >= this.length-1 || index <= 0) {
      return console.log('Index Out of Bound');
    }
    let previousNode = this.traverseList(index-1);
    // console.log(presentNode);
    let deleteNode = previousNode.next;
    presentNode = deleteNode.next;
    previousNode.next = presentNode;
    presentNode.prev = previousNode;
    this.length--;
    return this.printList();
  }

  reverseList() {
    let presentNode = this.tail;
    const array = [];
    while (presentNode !== null) {
      array.push(presentNode.value);
      presentNode = presentNode.prev;
    }
    return array;
  }

  reverseLinkedList() {
    let endNode = this.tail;
    const myLinkedList2 = new DoublyLinkedList(endNode);

    while (endNode !== null) {
      endNode = endNode.prev;
      myLinkedList2.append(endNode);
    }
    return myLinkedList2;
  }
}

const myLinkedList = new DoublyLinkedList(10);
myLinkedList.append(5);
myLinkedList.append(16);
// console.log(myLinkedList.printList());
myLinkedList.prepend(1);
console.log(myLinkedList.printList());
// myLinkedList.insert(2, 99);
// myLinkedList.insert(20, 88)
// console.log(myLinkedList.printList());

console.log(myLinkedList.traverseList(2));

// myLinkedList.removeNode(2);
// myLinkedList.removeNode(30);
// console.log(myLinkedList.printList());

// console.log(myLinkedList);
// console.log(myLinkedList.reverseList());
// console.log(myLinkedList.reverseLinkedList());