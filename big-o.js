const nemos = ['nemo'];

for (let index = 0; index < nemos.length; index++) {
  const t0=performance.now();
  const element = nemos[index];
  if (element == 'nemo') {
    console.log('Found nemo');
  }
  const t1=performance.now();
  console.log('Performance '+ (t1-t0) +' ms');
}