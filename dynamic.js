// Dynamic Programming

function multiplyTo10() {
    const cache = {}

    return function (num) {
        if (num in cache) {
            return cache[num];
        } else {
            cache[num] = num * 10;
            console.log("not in cache");
            return cache[num];
        }
    }
};


// dynamicProg = multiplyTo10()
// console.log(dynamicProg(5));
// console.log(dynamicProg(5));
// console.log(dynamicProg(5));
// console.log(dynamicProg(6));


//0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233...
let calculations = 0;


function fibonacciMaster() { //O(n)
    let cache = {};
    return function fib(n) {
        calculations++;
        if (n in cache) {
            return cache[n];
        } else {
            if (n < 2) {
                return n;
            } else {
                cache[n] = fib(n - 1) + fib(n - 2);
                return cache[n];
            }
        }
    }
}

// It is also dynamic programming. It is bottom up
function fibonacciMaster2(n) {
    let answer = [0, 1];
    for (let i = 2; i <= n; i++) {
        answer.push(answer[i - 2] + answer[i - 1]);
    }
    return answer.pop();
}

const fasterFib = fibonacciMaster();

// console.log('Slow', fibonacci(35))
// console.log('DP', fasterFib(100));
console.log('DP2', fibonacciMaster2(35));
console.log('we did ' + calculations + ' calculations');




let nums = [1, 2, 3, 1]
console.log(rob(nums));